package taskiful;

/* Data model for node objects. NoteViews are observers of Notes. */

import java.util.ArrayList;

public class Note implements Comparable {

    NoteView observer;
    String description;
    protected ArrayList<LabelView> labels;
    protected String name;

    public Note(String name, String description) {
        this.name = name;
        this.description = description;
        this.labels = new ArrayList<>();
    }

    /* Updates this note to match the given note and updates the observer */
    public void update(Note note) {
        this.name = note.getName();
        this.description = note.getDescription();
        this.labels = note.getLabels();
        updateObserver();
    }

    /* Removes the given label and updates the observer */
    void removeLabel(LabelView label) {
        this.labels.remove(label);
        updateObserver();
    }

    /* Deletes this note */
    void deleteNote() {
        this.observer.deleteNote();
    }

    /* Gets the name of the column this note it in */
    String getColumnName() {
        return this.observer.getColumnName();
    }

    /* Updates the observer */
    private void updateObserver() {
        observer.update(name, description, labels);
    }

    void setObserver(NoteView noteView) {
        this.observer = noteView;
    }

    NoteView getObserver() {
        return this.observer;
    }

    public String getName() {
        return name;
    }

    String getDescription() {
        return description;
    }

    public ArrayList<LabelView> getLabels() {
        return labels;
    }

    public void setLabels(ArrayList<LabelView> labels) {
        this.labels = labels;
    }

    /* Used to compare notes based on title */
    @Override
    public int compareTo(Object o) {
        Note note = (Note)o;
        if(this.name == null) return 1;
        if(note.name == null) return -1;
        return this.name.compareTo(note.name);
    }
}
