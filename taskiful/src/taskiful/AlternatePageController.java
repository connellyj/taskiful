package taskiful;

/* An abstract class that adds back button and close functionality
 * to all pages that open off of the main page. Requires that there
 * be a button with the fx:id backButton in the FXML file associated
 * with this controller.
 */

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public abstract class AlternatePageController {

    @FXML protected Button backButton;
    protected SessionController sessionController;
    private Node returnPane;

    public void initializeController(SessionController sessionController, Node returnPane) {
        this.sessionController = sessionController;
        this.returnPane = returnPane;
        initBackButton();
    }

    /* Adds a graphic to the back button */
    private void initBackButton() {
        Image back = new Image(getClass().getResourceAsStream("/img/backicon.png"));
        ImageView imageView = new ImageView(back);
        imageView.fitHeightProperty().setValue(20);
        imageView.fitWidthProperty().setValue(20);
        if(backButton != null) backButton.setGraphic(imageView);
        else System.err.println("Please include a button with fx:id = backButton in the fxml file");
    }

    /* Hides the back button */
    void deActivateBackButton() {
        backButton.setVisible(false);
    }

    /* Closes the page */
    @FXML protected void close() {
        sessionController.resetMainPane(returnPane);
    }
}
