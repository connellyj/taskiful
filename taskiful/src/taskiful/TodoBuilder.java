package taskiful;

/* A helper class to build to-do objects using the Builder method*/

import java.time.LocalDate;
import java.util.ArrayList;

public class TodoBuilder {

    private String name;
    private String description;
    private LocalDate date;
    private String priority;
    private ArrayList<Note> notes = new ArrayList<>();
    private ArrayList<Todo> todos = new ArrayList<>();

    public TodoBuilder setName(String name) {
        this.name = name;
        return this;
    }

    TodoBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    TodoBuilder setDate(LocalDate date) {
        this.date = date;
        return this;
    }

    TodoBuilder setPriority(String priority) {
        this.priority = priority;
        return this;
    }

    TodoBuilder setNotes(ArrayList<Note> notes) {
        this.notes = notes;
        return this;
    }

    void setTodos(ArrayList<Todo> todos) {
        this.todos = todos;
    }

    Todo createTodo() {
        return new Todo(name, description, date, priority, notes, todos);
    }
}
