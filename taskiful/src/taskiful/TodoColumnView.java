package taskiful;

/* Visually represents a column of to-do objects. Extends ColumnView.java */

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;

public class TodoColumnView extends ColumnView {

    private enum SortBy {
        DUE_DATE, PRIORITY
    }

    TodoColumnView(String name, SessionController sessionController) {
        super(name, sessionController);
    }

    /* Initializes the menu with all editing options */
    protected void initSettingsMenu(MenuButton button) {
        MenuItem addNewMenu = new MenuItem("Add New Todo");
        addNewMenu.setOnAction(event -> sessionController.createNewTodoInColumn(columnName.getText()));
        MenuItem deleteMenu = new MenuItem("Delete Column");
        deleteMenu.setOnAction(event -> sessionController.deleteTodoColumn(this));
        MenuItem changeName = new MenuItem("Rename Column");
        changeName.setOnAction(event -> {
            String name = sessionController.getColumnNameFromUser();
            if(name != null) setName(name);
        });
        MenuItem date = new MenuItem("Due Date");
        MenuItem priority = new MenuItem("Priority");
        Menu sort = new Menu("Sort by...");
        sort.getItems().addAll(date, priority);
        date.setOnAction(event -> sortBy(SortBy.DUE_DATE));
        priority.setOnAction(event -> sortBy(SortBy.PRIORITY));
        button.getItems().addAll(addNewMenu, changeName, sort, deleteMenu);
    }

    /* Sorts the todos in this column by the provided criteria */
    private void sortBy(SortBy sortBy) {
        ObservableList<Node> children = noteBox.getChildren();
        ArrayList<TodoView> todos = new ArrayList<>();
        for (Node child : children) todos.add((TodoView)child);
        switch (sortBy) {
            case PRIORITY:
                todos.sort((o1, o2) -> {
                    String todo1 = sessionController.getTodoModel(o1).getPriority();
                    String todo2 = sessionController.getTodoModel(o2).getPriority();
                    if(todo1 == null) return 1;
                    if(todo2 == null) return -1;
                    else return todo1.compareTo(todo2);
                });
                break;
            case DUE_DATE:
                todos.sort((o1, o2) -> {
                    LocalDate todo1 = sessionController.getTodoModel(o1).getLocalDate();
                    LocalDate todo2 = sessionController.getTodoModel(o2).getLocalDate();
                    if(todo1 == null) return 1;
                    if(todo2 == null) return -1;
                    else return todo1.compareTo(todo2);
                });
        }
        ObservableList<TodoView> sorted = FXCollections.observableArrayList(todos);
        noteBox.getChildren().clear();
        noteBox.getChildren().setAll(sorted);
    }

}
