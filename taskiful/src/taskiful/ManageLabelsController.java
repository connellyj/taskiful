package taskiful;

/* Controller for manageLabelsView.fxml. Handles user input
 * from the view and updates the label model as required.
 * Extends AlternatePageController.java.
 */

import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import java.util.ArrayList;
import java.util.Optional;

public class ManageLabelsController extends AlternatePageController {

    @FXML private VBox labelPane;
    @FXML private GridPane editorPane;
    private ArrayList<ColorPicker> colorPickers;

    public void initializeController(SessionController sessionController, Node returnPane) {
        super.initializeController(sessionController, returnPane);
        initView();
    }

    /* Adds each label to the view */
    private void initView() {
        colorPickers = new ArrayList<>();
        ArrayList<LabelView> labels = sessionController.getLabels();
        int index = 0;
        for(LabelView label : labels) {
            initLabelRow(label, index);
            index++;
        }
    }

    /* Adds one label plus associated buttons to the view */
    private void initLabelRow(LabelView label, int index) {
        labelPane.getChildren().add(label);
        GridPane.setHalignment(label, HPos.LEFT);
        Button nameButton = initNameButton(label, index);
        ColorPicker colorPicker = initBackgroundColorPicker(label, index);
        ColorPicker textColorPicker = initTextColorPicker(label, index);
        Button deleteButton = initDeleteButton(label, index, colorPicker, textColorPicker, nameButton);
        editorPane.getChildren().addAll(nameButton, colorPicker, textColorPicker, deleteButton);
    }

    /* Initializes the edit name button for one label */
    private Button initNameButton(LabelView label, int index) {
        Button nameButton = new Button("Edit text");
        nameButton.setOnAction(event -> {
            String newText = getTextFromUser(label.getName());
            if(newText != null) {
                label.setName(newText);
                sessionController.changeLabelName(label, newText);
            }
        });
        GridPane.setConstraints(nameButton, 0, index);
        GridPane.setHalignment(nameButton, HPos.RIGHT);
        return nameButton;
    }

    /* Initializes the delete button for one label */
    private Button initDeleteButton(LabelView label, int index, ColorPicker colorPicker, ColorPicker textColorPicker, Button nameButton) {
        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(event -> {
            sessionController.deleteLabels(label);
            labelPane.getChildren().remove(label);
            editorPane.getChildren().remove(colorPicker);
            editorPane.getChildren().remove(nameButton);
            editorPane.getChildren().remove(deleteButton);
            editorPane.getChildren().remove(textColorPicker);
        });
        GridPane.setConstraints(deleteButton, 3, index);
        GridPane.setHalignment(deleteButton, HPos.RIGHT);
        return deleteButton;
    }

    /* Initializes the background color picker for one label */
    private ColorPicker initBackgroundColorPicker(LabelView label, int index) {
        ColorPicker colorPicker = new ColorPicker();
        colorPickers.add(colorPicker);
        colorPicker.setValue(label.getColor());
        colorPicker.setOnAction(event -> {
            label.setColor(colorPicker.getValue());
            sessionController.changeLabelColor(label, colorPicker.getValue());
        });
        GridPane.setConstraints(colorPicker, 1, index);
        GridPane.setHalignment(colorPicker, HPos.RIGHT);
        return colorPicker;
    }

    /* Initializes the text color picker for one label */
    private ColorPicker initTextColorPicker(LabelView label, int index) {
        ColorPicker textColorPicker = new ColorPicker();
        colorPickers.add(textColorPicker);
        textColorPicker.setValue(label.getTextColor());
        textColorPicker.setOnAction(event -> {
            label.setTextColor(textColorPicker.getValue());
            sessionController.changeLabelTextColor(label, textColorPicker.getValue());
        });
        GridPane.setConstraints(textColorPicker, 2, index);
        GridPane.setHalignment(textColorPicker, HPos.RIGHT);
        return textColorPicker;
    }

    /* Creates a pop up to get a new string from the user */
    private String getTextFromUser(String oldText) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setGraphic(null);
        dialog.setHeaderText("Edit text:");
        dialog.getEditor().setText(oldText);
        Optional<String> result = dialog.showAndWait();
        return result.orElse(null);
    }

    /* Creates a new label and resets the view */
    @FXML private void createNewLabel() {
        sessionController.addNewLabel();
        initView();
    }

    /* Helper method to determine if any color pickers are selected */
    private boolean anyColorPickerSelected() {
        for(ColorPicker c : colorPickers) {
            if(c.isFocused()) return true;
        }
        return false;
    }

    /* If no color pickers are selected, close this page */
    @FXML protected void close() {
        if(!anyColorPickerSelected()) super.close();
    }
}
