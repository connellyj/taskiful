package taskiful;

/* Visually represents a column of notes. Extends ColumnView.java */

import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;

public class NoteColumnView extends ColumnView {

    NoteColumnView(String name, SessionController sessionController) {
        super(name, sessionController);
    }

    /* Initializes the menu with all editing options */
    protected void initSettingsMenu(MenuButton button) {
        MenuItem addNewMenu = new MenuItem("Add New Note");
        addNewMenu.setOnAction(event -> sessionController.createNewNoteInColumn(columnName.getText()));
        MenuItem deleteMenu = new MenuItem("Delete Column");
        deleteMenu.setOnAction(event -> sessionController.deleteNoteColumn(this));
        MenuItem changeName = new MenuItem("Rename Column");
        changeName.setOnAction(event -> {
            String name = sessionController.getColumnNameFromUser();
            if(name != null) setName(name);
        });
        button.getItems().addAll(addNewMenu, changeName, deleteMenu);
    }
}
