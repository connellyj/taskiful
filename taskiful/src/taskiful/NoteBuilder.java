package taskiful;

/* A helper class to build note objects using the Builder pattern */

public class NoteBuilder {

    private String name;
    private String description;

    public NoteBuilder setName(String name) {
        this.name = name;
        return this;
    }

    NoteBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    Note createNote() {
        return new Note(name, description);
    }
}
