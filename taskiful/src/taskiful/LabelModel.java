package taskiful;

/* Contains all the information for all labels in the application.
 * Each original label is a subject for all its copies which are observers.
 */

import javafx.scene.paint.Color;
import java.util.*;

public class LabelModel {

    private HashMap<LabelView, ArrayList<LabelView>> labels;
    private HashMap<LabelView, Note> labelLocations;

    LabelModel() {
        labels = new HashMap<>();
        labelLocations = new HashMap<>();
    }

    /* Adds a label by creating a label view and adding it to the model */
    void addLabel(String name, Color color, Color textColor) {
        labels.put(new LabelView(name, color, textColor), new ArrayList<>());
    }

    /* Gets all the labels */
    public Set<LabelView> getLabels() {
        return labels.keySet();
    }

    /* Gets all the labels sorted alphabetically by name */
    ArrayList<LabelView> getLabelsSorted() {
        ArrayList<LabelView> sorted = new ArrayList<>(getLabels());
        sorted.sort(Comparator.comparing(LabelView::getName));
        return sorted;
    }

    /* Given a set of LabelViews, creates copies of all of them, adds them to the model, and returns them */
    ArrayList<LabelView> copyLabels(ArrayList<LabelView> labels, Note note, boolean labelIsNotCopy) {
        ArrayList<LabelView> copies = new ArrayList<>();
        for(LabelView label : labels) {
            LabelView labelView = new LabelView(label.getName(), label.getColor(), label.getTextColor());
            copies.add(labelView);
            if(labelIsNotCopy) addLabelCopy(label, labelView, note);
            else addLabelCopyFromCopy(label, labelView, note);
        }
        return copies;
    }

    /* Given a set of LabelViews that were already copies,
     * creates copies of all of them, adds them to the model, and returns them
     */
    private void addLabelCopyFromCopy(LabelView labelCopy, LabelView labelCopyToAdd, Note note) {
        LabelView label = null;
        for(LabelView l : labels.keySet()) {
            if(l.equalTo(labelCopy)) {
                label = l;
                break;
            }
        }
        if(label != null) addLabelCopy(label, labelCopyToAdd, note);
    }

    /* Given a label and its copy, adds that association to the view */
    private void addLabelCopy(LabelView label, LabelView labelCopy, Note note) {
        labels.get(label).add(labelCopy);
        labelLocations.put(labelCopy, note);
    }

    /* For a given label, change all of its copies' colors to match its color */
    void changeLabelColor(LabelView label, Color color) {
        for(LabelView l : labels.get(label)) {
            l.setColor(color);
        }
    }

    /* For a given label, change all of its copies' text colors to match its text color */
    void changeLabelTextColor(LabelView label, Color color) {
        for(LabelView l : labels.get(label)) {
            l.setTextColor(color);
        }
    }

    /* For a given label, change all of its copies' names to match its name */
    void changeLabelName(LabelView label, String name) {
        for(LabelView l : labels.get(label)) {
            l.setName(name);
        }
    }

    /* For a given label, delete itself and all its copies */
    void deleteLabels(LabelView label) {
        for(LabelView l : labels.get(label)) {
            labelLocations.remove(l).removeLabel(l);
        }
        labels.remove(label);
    }
}
