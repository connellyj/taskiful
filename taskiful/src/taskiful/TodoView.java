package taskiful;

/* Represents a to-do object visually. Extends NoteView.java. */

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.MenuButton;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.control.MenuItem;
import java.util.ArrayList;

public class TodoView extends NoteView {

    private String date;
    private String priority;
    private ArrayList<Note> subNotes;
    private ArrayList<Todo> subTodos;

    TodoView(SessionController sessionController, ColumnView columnView,
             String name, String description, ArrayList<LabelView> labels,
             String date, String priority, ArrayList<Note> notes, ArrayList<Todo> todos) {
        super(sessionController, columnView, name, description, labels);
        this.date = date;
        this.priority = priority;
        this.subNotes = notes;
        this.subTodos = todos;
        initTodoView();
    }

    protected void initView() {
        super.initView();
        initTodoView();
    }

    /* Initializes to-do specific parameters in the view */
    private void initTodoView() {
        if (date != null) initDate();
        if (priority != null) initPriority();
        if (subNotes != null && subNotes.size() > 0) initSubNotes();
        if (subTodos != null && subTodos.size() > 0) initSubTodos();
    }

    /* Adds the date to the view */
    private void initDate() {
        Text dueText = new Text("Due Date: " + date);
        addRow(new HBox(dueText));
    }

    /* Adds the date to the view */
    private void initPriority() {
        Text priorityText = new Text("Priority: " + priority);
        addRow(new HBox(priorityText));
    }

    /* Adds the sub-notes to the view as links */
    private void initSubNotes() {
        Text attached = new Text("Attached notes: ");
        FlowPane flowPane = new FlowPane();
        flowPane.getChildren().add(attached);
        for(Note n : subNotes) {
            Hyperlink h = new Hyperlink(n.getName());
            h.setOnAction(event -> sessionController.displayNote(n.getObserver()));
            flowPane.getChildren().add(h);
        }
        addRow(new HBox(flowPane));
    }

    /* Adds the sub-todos to the view as links */
    private void initSubTodos() {
        Text attached = new Text("Sub-tasks: ");
        FlowPane flowPane = new FlowPane();
        flowPane.getChildren().add(attached);
        for(Todo t : subTodos) {
            Hyperlink h = new Hyperlink(t.getName());
            h.setOnAction(event -> sessionController.displayTodo((TodoView)t.getObserver()));
            flowPane.getChildren().add(h);
        }
        addRow(new HBox(flowPane));
    }

    /* Updates the view to match the given data */
    void update(String name, String description, ArrayList<LabelView> labels,
                String date, String priority, ArrayList<Note> subNotes, ArrayList<Todo> subTodos) {
        this.date = date;
        this.priority = priority;
        this.subNotes = subNotes;
        this.subTodos = subTodos;
        super.update(name, description, labels);
    }

    /* Adds to-do specific functionality to the menu */
    protected void initEditButton() {
        super.initEditButton();
        MenuItem markCompleted = new MenuItem("Mark as Completed");
        markCompleted.setOnAction(event -> sessionController.markAsCompleted(this));
        addMenuOption(markCompleted);
    }


    /* Initializes the button used if this to-do is in the completed section */
    void initCompletedEditButton(){
        editButton = new MenuButton();
        MenuItem moveTodosMenu = new MenuItem("Move to Todos");
        moveTodosMenu.setOnAction(event -> sessionController.unmarkCompleted(this));
        MenuItem deleteMenu = new MenuItem("Delete");
        deleteMenu.setOnAction(event -> sessionController.deleteCompleted(this));
        addMenuOption(moveTodosMenu);
        addMenuOption(deleteMenu);
        StackPane.setAlignment(editButton, Pos.TOP_RIGHT);
        StackPane stackPane = new StackPane(editButton);
        stackPane.setPadding(new Insets(0.0, 0.0, 0.0, 10.0));
        borderPane.setRight(stackPane);
    }

    /* Gets the name of the column this todo it in */
    public String getColumnName() {
        return columnView.getName();
    }

}
