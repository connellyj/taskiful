package taskiful;

/* Represents a note object visually */

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import java.util.ArrayList;

public class NoteView  extends StackPane {

    BorderPane borderPane;
    ColumnView prevColumn;
    MenuButton editButton;
    ColumnView columnView;
    protected SessionController sessionController;
    protected String name;
    protected ArrayList<LabelView> labels;
    private String description;
    private VBox noteDataPane;

    NoteView(SessionController sessionController, ColumnView columnView,
             String name, String description, ArrayList<LabelView> labels) {
        this.columnView = columnView;
        this.sessionController = sessionController;
        this.name = name;
        this.description = description;
        this.labels = labels;
        initMainPane();
        initStyle();
        initEditButton();
        initView();
        initClickAndDrag();
    }

    /* Initializes the framework of this visual object */
    private void initMainPane() {
        noteDataPane = new VBox();
        borderPane = new BorderPane();
        borderPane.setCenter(noteDataPane);
        borderPane.setOnMouseClicked(event -> sessionController.openItemInNewPage(this));
        this.getChildren().add(borderPane);
    }

    /* Puts all the data into the view */
    protected void initView() {
        if(name != null) initName();
        if(description != null) initDescription();
        if(labels != null && labels.size() > 0) initLabels();
    }

    /* Adds the name to the view */
    private void initName() {
        Text nameText = new Text(name);
        nameText.setStyle("-fx-font-size: 18; -fx-font-weight: bold");
        TextFlow textFlow = new TextFlow(nameText);
        addRow(new HBox(textFlow));
    }

    /* Adds the description to the view */
    private void initDescription() {
        Text descriptionText;
        if(description.length() > 80) descriptionText = new Text(description.substring(0, 80) + "...");
        else descriptionText = new Text(description);
        TextFlow textFlow = new TextFlow(descriptionText);
        addRow(new HBox(textFlow));
    }

    /* Adds the labels to the view */
    private void initLabels() {
        Text labelText = new Text("Labels: ");
        FlowPane labelPane = new FlowPane();
        labelPane.getChildren().addAll(labels);
        addRow(new HBox(labelText, labelPane));
    }

    /* Initializes click and drag functionality */
    private void initClickAndDrag() {
        this.setOnDragDetected(e -> sessionController.setMovingView(this));
    }

    /* Initializes the style of this view */
    private void initStyle() {
        this.setPadding(new Insets(10.0, 10.0, 10.0, 10.0));
        this.setPrefWidth(ColumnView.MIN_NOTE_WIDTH);
        noteDataPane.setSpacing(10.0);
        borderPane.setStyle("-fx-border-color: black; -fx-border-width: 2px; -fx-background-color: #e9e9ec");
        borderPane.setPadding(new Insets(10.0, 10.0, 10.0, 10.0));
    }

    /* Initializes the edit button with all menu options */
    protected void initEditButton() {
        editButton = new MenuButton();
        MenuItem editMenu = new MenuItem("Edit");
        MenuItem deleteMenu = new MenuItem("Delete");
        editMenu.setOnAction(event -> sessionController.editItem(this));
        deleteMenu.setOnAction(event -> deleteNote());
        addMenuOption(editMenu);
        addMenuOption(deleteMenu);
        StackPane.setAlignment(editButton, Pos.TOP_RIGHT);
        StackPane stackPane = new StackPane(editButton);
        stackPane.setPadding(new Insets(0.0, 0.0, 0.0, 10.0));
        borderPane.setRight(stackPane);
    }

    /* Deletes this note from its column */
    void deleteNote() {
        columnView.removeItemView(this);
    }

    /* Adds a menu option to the settings menu */
    void addMenuOption(MenuItem menuItem) {
        editButton.getItems().add(menuItem);
    }

    /* Adds a row of information to the view */
    void addRow(HBox data) {
        noteDataPane.getChildren().add(data);
    }

    /* Updates the view to match the new information */
    void update(String name, String description, ArrayList<LabelView> labels) {
        this.name = name;
        this.description = description;
        this.labels = labels;
        reset();
        initView();
    }

    /* Removes everything from the view and starts over */
    private void reset() {
        noteDataPane.getChildren().clear();
        initStyle();
        initEditButton();
    }

    /* Returns the name of the column this note is in */
    public String getColumnName() {
        return columnView.getName();
    }
}
