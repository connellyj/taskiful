package taskiful;

/* The controller for newNoteView.fxml. Handles
 * creating and saving note objects. Extends NewItemController.java.
 */

import javafx.fxml.FXML;

public class NewNoteController extends NewItemController{

    /* Saves the current note and closes the page */
    protected void saveAndClose(Note note) {
        note.update(createNote());
        close();
    }

    /* Creates a new note and closes the page */
    @FXML private void createNoteAndExit() {
        sessionController.addNoteToColumn(createNote(), itemColumnNames.getSelectionModel().getSelectedIndex());
        close();
    }

    /* Creates a new note using a NoteBuilder and all the information in the editor */
    private Note createNote() {
        NoteBuilder noteBuilder = new NoteBuilder();

        String descriptionString = itemDescription.getText();
        if(!descriptionString.equals("")) noteBuilder.setDescription(descriptionString);

        String titleString = itemTitle.getText();
        if(!titleString.equals("")) noteBuilder.setName(titleString);

        Note note = noteBuilder.createNote();

        if(selectedLabels.size() > 0) {
            note.setLabels(sessionController.copyLabels(selectedLabels, note, true));
        }

        return note;
    }
}
