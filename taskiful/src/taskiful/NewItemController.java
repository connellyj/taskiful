package taskiful;

/* An abstract class that supplies editing/creating
 * functionality that is identical for todos and notes.
 * Extends AlternatePageController.java.
 */

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;
import java.util.ArrayList;

public abstract class NewItemController extends AlternatePageController{

    @FXML protected ComboBox<String> itemColumnNames;
    @FXML protected FlowPane itemLabels;
    @FXML protected TextArea itemDescription;
    @FXML protected TextField itemTitle;
    @FXML protected Button createButton;
    @FXML protected Text headerText;
    ArrayList<LabelView> selectedLabels;

    public void initializeController(SessionController sessionController, Node returnPane, ArrayList<String> columnNames, ArrayList<LabelView> labels) {
        super.initializeController(sessionController, returnPane);
        initColumnNames(columnNames);
        initLabels(labels);
    }

    /* Puts all the column names into the column selector */
    private void initColumnNames(ArrayList<String> columnNames) {
        itemColumnNames.getItems().setAll(columnNames);
        itemColumnNames.getSelectionModel().select(0);
    }

    /* Associates all the labels with checkboxes and adds them to the view */
    private void initLabels(ArrayList<LabelView> labels) {
        ArrayList<CheckBox> checkBoxes = new ArrayList<>();
        selectedLabels = new ArrayList<>();
        for(LabelView label : labels) {
            CheckBox checkBox = new CheckBox();
            checkBox.setGraphic(label);
            checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
                if(newValue) selectedLabels.add(label);
                else selectedLabels.remove(label);
            });
            checkBoxes.add(checkBox);
        }
        itemLabels.getChildren().addAll(checkBoxes);
    }

    /* Selects the given column in the column selector */
    void selectColumn(String name) {
        itemColumnNames.getSelectionModel().select(name);
    }

    /* Sets the view up in editing mode */
    public void initEditMode(Note note, String column) {
        headerText.setText("Edit Note");
        createButton.setText("Save");
        createButton.setOnAction(event -> saveAndClose(note));
        if(note.getDescription() != null) itemDescription.setText(note.getDescription());
        if(note.getName() != null) itemTitle.setText(note.getName());
        if(note.getLabels() != null) selectLabels(note.getLabels());
        selectColumn(column);
    }

    /* Selects the checkboxes next to all the given labels */
    private void selectLabels(ArrayList<LabelView> labels) {
        for(Node child : itemLabels.getChildren()) {
            CheckBox c = (CheckBox) child;
            LabelView cur = (LabelView) c.getGraphic();
            for(LabelView label : labels) {
                if(cur.equalTo(label)) {
                    c.setSelected(true);
                }
            }
        }
    }

    /* Creates a new label and refreshes the labels */
    @FXML private void createNewLabel() {
        sessionController.addNewLabel();
        itemLabels.getChildren().clear();
        ArrayList<LabelView> prevSelected = selectedLabels;
        initLabels(sessionController.getLabels());
        selectLabels(prevSelected);
    }

    /* All subclasses must be able to save and close an edited note */
    protected abstract void saveAndClose(Note note);
}
