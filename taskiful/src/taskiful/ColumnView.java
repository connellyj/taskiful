package taskiful;

/* An abstract class that visually represents a column in the task manager */

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.MenuButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public abstract class ColumnView extends VBox {

    /* Used to regulate column widths across all columns */
    private static final double MIN_COLUMN_WIDTH = 300;
    static final double MIN_NOTE_WIDTH = MIN_COLUMN_WIDTH - 12;

    Text columnName;
    VBox noteBox;
    protected SessionController sessionController;

    ColumnView(String name, SessionController sessionController) {
        this.sessionController = sessionController;
        initHeader(name);
        initNoteBox();
        initStyle();
        initClickAndDrag();
    }

    /* Sets up style for the column */
    private void initStyle() {
        this.setStyle("-fx-border-color: black; -fx-border-width: 2px");
        this.setPrefWidth(MIN_COLUMN_WIDTH);
    }

    /* Adds event handlers for click and drag functionality */
    private void initClickAndDrag() {
        this.setOnMouseReleased(event -> sessionController.changeColumn());
        this.setOnMouseEntered(event -> sessionController.setMovingColumn(this));
    }

    /* Creates and fills in the header of the column */
    private void initHeader(String name) {
        columnName = new Text(name);
        columnName.setStyle("-fx-font-size: 18; -fx-font-weight: bold");
        TextFlow textFlow = new TextFlow(columnName);
        Image gear = new Image(getClass().getResourceAsStream("/img/gear.png"));
        ImageView imageView = new ImageView(gear);
        imageView.fitHeightProperty().setValue(20);
        imageView.fitWidthProperty().setValue(20);
        MenuButton settingsButton = new MenuButton();
        settingsButton.setGraphic(imageView);
        initSettingsMenu(settingsButton);
        HBox hBox = new HBox(textFlow, settingsButton);
        hBox.setSpacing(10.0);
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(10.0, 10.0, 10.0, 10.0));
        hBox.setStyle("-fx-background-color: lightblue");
        this.getChildren().add(hBox);
    }

    /* Adds a scroll pane around the part of the column that contains notes/todos */
    private void initNoteBox() {
        AnchorPane anchorPane = new AnchorPane();
        VBox.setVgrow(anchorPane, Priority.ALWAYS);
        this.getChildren().add(anchorPane);
        ScrollPane scrollPane = new ScrollPane();
        AnchorPane.setBottomAnchor(scrollPane, 0.0);
        AnchorPane.setLeftAnchor(scrollPane, 0.0);
        AnchorPane.setRightAnchor(scrollPane, 0.0);
        AnchorPane.setTopAnchor(scrollPane, 0.0);
        anchorPane.getChildren().addAll(scrollPane);
        noteBox = new VBox();
        scrollPane.setContent(noteBox);
    }

    /* Sets the name of the column to the provided String */
    public void setName(String name) {
        columnName.textProperty().setValue(name);
    }

    /* Returns the name of the column */
    public String getName() {
        return columnName.getText();
    }

    /* Adds the given pane to the bottom of the list of notes/todos */
    void addToEnd(Pane item) {
        noteBox.getChildren().add(item);
    }

    /* Adds the given pane to the beginning of the list of notes/todos */
    void addToBeginning(Pane item) {noteBox.getChildren().add(0, item); }

    /* Removes the given pane from this column */
    void removeItemView(Pane view) {
        noteBox.getChildren().remove(view);
    }

    /* Requires all columns to initialize their own settings menu with dedicated options */
    protected abstract void initSettingsMenu(MenuButton button);
}
