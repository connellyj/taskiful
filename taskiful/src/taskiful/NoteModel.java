package taskiful;

/* Data model for all the notes/todos in the application */

import java.util.*;

class NoteModel {

    private HashMap<NoteView, Note> notes;
    private HashMap<TodoView, Todo> todos;

    NoteModel() {
        notes = new HashMap<>();
        todos = new HashMap<>();
    }

    Note getNoteModel(NoteView view) {
        return notes.get(view);
    }

    Todo getTodoModel(TodoView view) {
        return todos.get(view);
    }

    void addNote(NoteView view, Note note) {
        notes.put(view, note);
    }

    void addTodo(TodoView view, Todo todo) {
        todos.put(view, todo);
    }

    private Collection<Note> getNotes() {
        return notes.values();
    }

    private Collection<Todo> getTodos() {
        return todos.values();
    }

    ArrayList<Note> getNotesSorted() {
        ArrayList<Note> names = new ArrayList<>(getNotes());
        names.sort(Note::compareTo);
        return names;
    }

    ArrayList<Todo> getTodosSorted() {
        ArrayList<Todo> names = new ArrayList<>(getTodos());
        names.sort(Todo::compareTo);
        return names;
    }
}
