package taskiful;

/* The controller for displayNoteView.fxml. Fills in the available fields
 * and responds to user input coming from the view.Extends from
 * AlternatePageController.java
 */

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;

public class DisplayNoteController extends AlternatePageController {

    @FXML protected Button deleteButton;
    @FXML protected FlowPane labelPane;
    @FXML protected Text descriptionText;
    @FXML protected Text titleText;
    private Note note;
    private DisplayTodoController parent;

    public void initializeController(SessionController sessionController, Node returnPane, Note note) {
        super.initializeController(sessionController, returnPane);
        this.note = note;
        initView();
    }

    /* Fills in all available fields with data if the data exists */
    protected void initView() {
        if(note != null) {
            if (note.getDescription() != null) descriptionText.setText(note.getDescription());
            if (note.getName() != null) titleText.setText(note.getName());
            labelPane.getChildren().clear();
            if (note.getLabels().size() > 0)
                labelPane.getChildren().addAll(sessionController.copyLabels(note.getLabels(), note, false));
        }
    }

    /* Opens the edit note page */
    @FXML protected void editNote() {
        sessionController.editItem(note, this);
    }

    /* Deletes this note and closes the page */
    @FXML private void deleteNote() {
        note.deleteNote();
        close();
    }

    /* Changes the delete button to a remove button which
     * removes this note/to-do from its parent to-do */
    void changeDeleteToRemove(Todo todo) {
        this.deleteButton.setText("Remove");
        this.deleteButton.setOnAction(event -> {
            todo.removeSubItem(this.note);
            parent.update();
        });
    }

    /* Sets the parent of this note/to-do object */
    void setParentTodo(DisplayTodoController controller) {
        this.parent = controller;
    }

    /* Resets the view */
    void update() {
        initView();
    }
}
