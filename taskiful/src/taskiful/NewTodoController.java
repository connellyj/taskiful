package taskiful;

/* The controller for newTodoView.fxml. Handles
 * creating and saving to-do objects. Extends NewItemController.java.
 */

import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import java.time.LocalDate;
import java.util.ArrayList;

public class NewTodoController extends NewItemController {

    @FXML private DatePicker todoDate;
    @FXML private ComboBox<String> todoPriority;
    private ArrayList<Note> selectedNotes;
    private ArrayList<Todo> selectedTodos;
    private Todo editing;
    private DisplayNoteController observer;

    public void initializeController(SessionController sessionController, Node returnPane, ArrayList<String> columnNames, ArrayList<LabelView> labels) {
        super.initializeController(sessionController, returnPane, columnNames, labels);
        initPriority();
        selectedNotes = new ArrayList<>();
        selectedTodos = new ArrayList<>();
    }

    /* Sets a display controller as an observer */
    void setObserver(DisplayNoteController controller) {
        this.observer = controller;
    }

    /* Adds priority values to the priority selector */
    private void initPriority() {
        ArrayList<String> p = new ArrayList<>();
        for(Integer i = 1; i < 10; i++) {
            p.add(i.toString());
        }
        p.set(0, p.get(0) + " (max)");
        p.set(8, p.get(8) + " (min)");
        todoPriority.getItems().setAll(p);
    }

    /* Sets up the view in editing mode */
    public void initEditMode(Note note, String column) {
        super.initEditMode(note, column);
        Todo todo = (Todo)note;
        editing = todo;
        headerText.setText("Edit Todo");
        if(todo.getLocalDate() != null) todoDate.setValue(todo.getLocalDate());
        if(todo.getPriority() != null) todoPriority.getSelectionModel().select(todo.getPriority());
        if(todo.getSubNotes().size() > 0) selectedNotes = todo.getSubNotes();
        if(todo.getSubTodos().size() > 0) selectedTodos = todo.getSubTodos();
    }

    /* Saves the current to-do and closes the page */
    protected void saveAndClose(Note note) {
        Todo todo = (Todo)note;
        todo.update(createTodo());
        if(observer != null) observer.update();
        close();
    }

    /* Creates a to-do object and closes the page */
    @FXML private void createTodoAndExit() {
        sessionController.addTodoToColumn(createTodo(), itemColumnNames.getSelectionModel().getSelectedIndex());
        close();
    }

    /* Opens a dialog to get notes the user wants to attach to this to-do */
    @FXML private void handleAttachNote() {
        ChoiceDialog<Note> dialog = new ChoiceDialog<>();
        dialog.setGraphic(null);
        dialog.setTitle("Attach Notes");
        dialog.setHeaderText("Choose notes to attach:");
        dialog.getDialogPane().setPrefSize(400, 200);
        FlowPane flowPane = new FlowPane();
        flowPane.setHgap(10.0);
        flowPane.setVgap(10.0);
        flowPane.setOrientation(Orientation.VERTICAL);
        ArrayList<Note> notes = sessionController.getNotes();
        for(Note note : notes) {
            CheckBox checkBox = new CheckBox(note.getName());
            checkBox.setSelected(selectedNotes.contains(note));
            checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
                if(newValue) selectedNotes.add(note);
                else selectedNotes.remove(note);
            });
            flowPane.getChildren().add(checkBox);
        }
        dialog.getDialogPane().setContent(flowPane);
        dialog.showAndWait();
    }

    /* Opens a dialog to get todos the user wants to attach to this to-do */
    @FXML private void handleAttachTodo() {
        ChoiceDialog<Note> dialog = new ChoiceDialog<>();
        dialog.setGraphic(null);
        dialog.setTitle("Attach Sub-Todos");
        dialog.setHeaderText("Choose todos to attach:");
        dialog.getDialogPane().setPrefSize(500, 300);
        FlowPane flowPane = new FlowPane();
        flowPane.setHgap(10.0);
        flowPane.setVgap(10.0);
        flowPane.setOrientation(Orientation.VERTICAL);
        ArrayList<Todo> todos = sessionController.getTodos();
        for(Todo todo : todos) {
            if(todo != editing) {
                CheckBox checkBox = new CheckBox(todo.getName());
                checkBox.setSelected(selectedTodos.contains(todo));
                checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
                    if(newValue) selectedTodos.add(todo);
                    else selectedTodos.remove(todo);
                });
                flowPane.getChildren().add(checkBox);
            }
        }
        dialog.getDialogPane().setContent(flowPane);
        dialog.showAndWait();
    }

    /* Creates a to-do using a TodoBuilder and all the information in the editor */
    private Todo createTodo() {
        TodoBuilder todoBuilder = new TodoBuilder();

        String descriptionString = itemDescription.getText();
        if(!descriptionString.equals("")) todoBuilder.setDescription(descriptionString);

        String titleString = itemTitle.getText();
        if(!titleString.equals("")) todoBuilder.setName(titleString);

        if(todoDate.getValue() != null) {
            LocalDate date = todoDate.getValue();
            todoBuilder.setDate(date);
        }

        if(!todoPriority.getSelectionModel().isEmpty()) {
            String priorityString = todoPriority.getSelectionModel().getSelectedItem();
            todoBuilder.setPriority(priorityString.substring(0, 1));
        }

        if(selectedNotes.size() > 0) todoBuilder.setNotes(selectedNotes);

        if(selectedTodos.size() > 0) todoBuilder.setTodos(selectedTodos);

        Todo todo =  todoBuilder.createTodo();

        if(selectedLabels.size() > 0) {
            todo.setLabels(sessionController.copyLabels(selectedLabels, todo, true));
        }

        return todo;
    }
}
