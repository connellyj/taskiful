package taskiful;

/* Data model for to-do objects. TodoViews are observers of Todos. */

import java.time.LocalDate;
import java.util.ArrayList;

public class Todo extends Note {

    private String date;
    private String priority;
    private LocalDate localDate;
    private ArrayList<Note> subNotes;
    private ArrayList<Todo> subTodos;

    public Todo(String name, String description, LocalDate date, String priority, ArrayList<Note> notes, ArrayList<Todo> todos) {
        super(name, description);
        if(date != null) this.date = date.toString();
        this.localDate = date;
        this.priority = priority;
        this.subNotes = notes;
        this.subTodos = todos;
    }

    /* Updates this to-do to match the given to-do and updates the observer */
    public void update(Note note) {
        assert note instanceof Todo;
        Todo todo = (Todo) note;
        this.name = todo.getName();
        this.description = todo.getDescription();
        this.labels = todo.getLabels();
        this.date = todo.getDate();
        this.priority = todo.getPriority();
        this.localDate = todo.getLocalDate();
        this.subNotes = todo.getSubNotes();
        this.subTodos = todo.getSubTodos();
        updateObserver();
    }

    /* Updates the observer */
    private void updateObserver() {
        assert observer instanceof TodoView;
        ((TodoView) observer).update(name, description, labels, date, priority, subNotes, subTodos);
    }

    /* Removes a sub note/to-do from this to-do and updates the observer */
    void removeSubItem(Note n) {
        if(n instanceof Todo) {
            this.subTodos.remove(n);
        }else this.subNotes.remove(n);
        updateObserver();
    }

    String getDate(){
        return this.date;
    }

    String getPriority(){
        return this.priority;
    }

    LocalDate getLocalDate() {
        return  this.localDate;
    }

    ArrayList<Note> getSubNotes() {
        return this.subNotes;
    }

    ArrayList<Todo> getSubTodos() {
        return this.subTodos;
    }
}
