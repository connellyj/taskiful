package taskiful;

/* The controller for mainView.fxml. This controller is the central
 * controller for the entire application. It handles communication
 * between pretty much all other objects in the application.
 */

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

public class SessionController {

    @FXML private BorderPane mainBorderPane;
    @FXML private MenuButton newButton;
    @FXML private HBox todoTabPane;
    @FXML private HBox noteTabPane;
    @FXML private FlowPane completedTabPane;

    private ArrayList<NoteColumnView> noteColumns;
    private ArrayList<TodoColumnView> todoColumns;
    private LabelModel labelModel;
    private NoteModel noteModel;
    private NoteView movingView = null;
    private ColumnView movingColumn = null;

    private static final String TODO_FXML_PATH = "/fxml/newTodoView.fxml";
    private static final String NOTE_FXML_PATH = "/fxml/newNoteView.fxml";
    private static final String LABEL_FXML_PATH = "/fxml/manageLabelsView.fxml";
    static final String DISPLAY_TODO_PATH = "/fxml/displayTodoView.fxml";
    static final String DISPLAY_NOTE_PATH = "/fxml/displayNoteView.fxml";

    public enum PageType {
        TODO, NOTE, LABEL
    }

    public void initialize() {
        initNewItemButton();
        labelModel = new LabelModel();
        noteModel = new NoteModel();
        noteColumns = new ArrayList<>();
        todoColumns = new ArrayList<>();
        initializeExamples();

    }

    private void initializeExamples(){

        // Create example columns and todos.
        createNoteColumn("Example Note Column Title");
        createNoteColumn("Other Notes!");
        createTodoColumn("Example Todo Column Title");
        createTodoColumn("Other Todos!");


        // Create new labels, then make a list of those labels to easily add to future notes/todos.
        labelModel.addLabel("Finals", Color.AQUA, Color.BLACK);
        labelModel.addLabel("Self-Care", Color.PALEGOLDENROD, Color.BLACK);
        labelModel.addLabel("Projects", Color.RED, Color.BLUE);

        ArrayList<LabelView> finals = new ArrayList<>();
        finals.add(labelModel.getLabelsSorted().get(0));


        ArrayList<LabelView> selfCare = new ArrayList<>();
        selfCare.add(labelModel.getLabelsSorted().get(2));

        ArrayList<LabelView> projects = new ArrayList<>();
        projects.add(labelModel.getLabelsSorted().get(0));
        projects.add(labelModel.getLabelsSorted().get(1));

        // Create noteExamples and todoExample lists for appending "subnotes/todos".
        ArrayList<Note> noteExamples = new ArrayList<>();
        ArrayList<Todo> todoExamples = new ArrayList<>();

        // Create a few sample nodes, and add them to the two note example columns.

        Note exampleNote1 = new Note("Don't forget the README", "IMPORTANT");
        ArrayList<LabelView> ex1Labels = labelModel.copyLabels(projects, exampleNote1, true);
        exampleNote1.setLabels(ex1Labels);
        addNoteToColumn(exampleNote1, 0);

        Note exampleNote2 = new Note("Be sure to relax after finals!", "");

        ArrayList<LabelView> ex2Labels = labelModel.copyLabels(selfCare, exampleNote2, true);
        exampleNote2.setLabels(ex2Labels);
        addNoteToColumn(exampleNote2, 1);

        ArrayList<Note> finalNotes = new ArrayList<>();
        finalNotes.add(exampleNote1);

        // Create a few sample todos, and add them to the two todo example columns.

        Todo exampleTodo1 = new Todo("Finish CS257 Final Project", "Do this by 5 pm! :)",
                LocalDate.of(2017, 3, 15),"1", finalNotes, todoExamples);

        exampleTodo1.setLabels(ex1Labels);

        Todo subTodo1 = new Todo("Buy New Folders", "Target has a Sale!", null, "7",
                noteExamples, todoExamples);

        Todo subTodo2 = new Todo("Buy Snacks", "Cub Foods has coupons", null, "5",
                noteExamples, todoExamples);

        Todo subTodo3 = new Todo("Buy Notebooks", "", null, "6", noteExamples, todoExamples);

        Todo subTodo4 = new Todo("Buy Pencils", "", null, "4", noteExamples, todoExamples);

        // Create an example completed to-do.
        Todo completedExample = new Todo("Finish Other Final Paper", "Due by 5 pm!", LocalDate.of(2017, 3, 15),
                "2", noteExamples, todoExamples);
        completedExample.setLabels(ex1Labels);

        addTodoToColumn(completedExample, 0);

        markAsCompleted((TodoView)completedExample.getObserver());

        ArrayList<Todo> newTodos = new ArrayList<>();
        newTodos.add(subTodo1);
        newTodos.add(subTodo2);
        newTodos.add(subTodo3);
        newTodos.add(subTodo4);
        Todo exampleTodo2 = new Todo("Prep for Spring Term", "See Sub-Todos",
                LocalDate.of(2017, 3, 25), "5", noteExamples, newTodos);
        addTodoToColumn(exampleTodo1, 0);
        addTodoToColumn(exampleTodo2, 1);
        addTodoToColumn(subTodo1, 1);
        addTodoToColumn(subTodo2, 1);
        addTodoToColumn(subTodo3, 1);
        addTodoToColumn(subTodo4, 1);
    }

    /* Adds a grahic to the new item button */
    private void initNewItemButton() {
        Image plus = new Image(getClass().getResourceAsStream("/img/plus.png"));
        ImageView imageView = new ImageView(plus);
        imageView.fitHeightProperty().setValue(20);
        imageView.fitWidthProperty().setValue(20);
        newButton.setGraphic(imageView);
    }

    /* Creates a note column and adds it to the view */
    private void createNoteColumn(String name) {
        NoteColumnView view = new NoteColumnView(name, this);
        noteTabPane.getChildren().add(0, view);
        noteColumns.add(view);
    }

    /* Creates a to-do column and adds it to the view */
    private void createTodoColumn(String name) {
        TodoColumnView view = new TodoColumnView(name, this);
        todoTabPane.getChildren().add(0, view);
        todoColumns.add(view);
    }

    /* Deletes the given column from the view */
    void deleteTodoColumn(TodoColumnView columnView) {
        todoTabPane.getChildren().remove(columnView);
        todoColumns.remove(columnView);
    }

    /* Deletes the given column from the view */
    void deleteNoteColumn(NoteColumnView columnView) {
        noteTabPane.getChildren().remove(columnView);
        noteColumns.remove(columnView);
    }

    /* Removes the given view from the completed tab */
    void deleteCompleted(TodoView view){
        completedTabPane.getChildren().removeAll(view);
    }


    /* Creates a note column with the name provided by the user */
    @FXML private void createNoteColumn() {
        String name = getColumnNameFromUser();
        if(name != null) createNoteColumn(name);
    }

    /* Creates a to-do column with the name provided by the user */
    @FXML private void createTodoColumn() {
        String name = getColumnNameFromUser();
        if(name != null) createTodoColumn(name);
    }

    /* Creates a pop up to get a column name from the user */
    String getColumnNameFromUser() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setGraphic(null);
        dialog.setTitle("New Column");
        dialog.setHeaderText("Name the column:");
        Optional<String> result = dialog.showAndWait();
        return result.orElse(null);
    }

    /* Asks the user to name a column */
    private String handleNoColumns() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setGraphic(null);
        dialog.setTitle("New Column");
        dialog.setHeaderText("You must create a column first:");
        Optional<String> result = dialog.showAndWait();
        return result.orElse(null);
    }

    /* Opens the new to-do page with the given column selected */
    void createNewTodoInColumn(String columnName) {
        NewTodoController newTodoController = (NewTodoController) openPage(PageType.TODO);
        if(newTodoController != null) newTodoController.selectColumn(columnName);
    }

    /* Opens the new note page with the given column selected */
    void createNewNoteInColumn(String columnName) {
        NewNoteController newNoteController = (NewNoteController) openPage(PageType.NOTE);
        if(newNoteController != null) newNoteController.selectColumn(columnName);
    }

    /* If there are no to-do columns, asks the user to make one, then open the new to-do screen */
    @FXML private void handleNewTodoButton() {
        if(todoColumns.size() == 0) {
            String column = handleNoColumns();
            if(column == null) return;
            else createTodoColumn(column);
        }
        openPage(PageType.TODO);
    }

    /* If there are no note columns, asks the user to make one, then opens the new note screen */
    @FXML private void handleNewNoteButton() {
        if(noteColumns.size() == 0) {
            String column = handleNoColumns();
            if(column == null) return;
            else createNoteColumn(column);
        }
        openPage(PageType.NOTE);
    }

    /* Opens the label manager page */
    @FXML private void openLabelManager() {
        openPage(PageType.LABEL);
    }

    /* Opens the item in a new page based on whether its a to-do or a note */
    void openItemInNewPage(NoteView view) {
        if(view instanceof TodoView) {
            openDisplayPage(view, PageType.TODO);
        }else {
            openDisplayPage(view, PageType.NOTE);
        }
    }

    /* Opens the given page */
    private AlternatePageController openPage(PageType pageType) {
        switch (pageType){
            case LABEL:
                return openPage(LABEL_FXML_PATH);
            case NOTE:
                return openNewItemPage(NOTE_FXML_PATH);
            case TODO:
                return openNewItemPage(TODO_FXML_PATH);
        }
        return null;
    }

    /* Opens one of the new item pages and initializes its controller */
    private NewItemController openNewItemPage(String fxmlPath) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fxmlPath));
            fxmlLoader.load();
            NewItemController controller = fxmlLoader.getController();
            if (controller instanceof NewTodoController) {
                controller.initializeController(this, getReturnPage(), getTodoColumnNames(), getLabels());
            } else {
                controller.initializeController(this, getReturnPage(), getNoteColumnNames(), getLabels());
            }
            Pane root = fxmlLoader.getRoot();
            mainBorderPane.setCenter(root);
            return controller;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error loading view at " + fxmlPath);
            return null;
        }
    }

    /* Displays the given page */
    private void openDisplayPage(NoteView view, PageType type) {
        switch (type) {
            case TODO:
                displayTodo((TodoView) view);
                break;
            case NOTE:
                displayNote(view);
                break;
            case LABEL:
                openLabelManager();
        }
    }

    /* Displays the given note in a new page */
    void displayNote(NoteView view) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(DISPLAY_NOTE_PATH));
            fxmlLoader.load();
            DisplayNoteController controller = fxmlLoader.getController();
            controller.initializeController(this, getReturnPage(), getNoteModel(view));
            Pane root = fxmlLoader.getRoot();
            mainBorderPane.setCenter(root);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error loading view at " + DISPLAY_NOTE_PATH);
        }
    }

    /* Displays the given to-do in a new page */
    void displayTodo(TodoView view) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(DISPLAY_TODO_PATH));
            fxmlLoader.load();
            DisplayTodoController controller = fxmlLoader.getController();
            controller.initializeController(this, getReturnPage(), getTodoModel(view));
            Pane root = fxmlLoader.getRoot();
            mainBorderPane.setCenter(root);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error loading view at " + DISPLAY_TODO_PATH);
        }
    }

    /* Opens the given page and initializes its controller */
    private AlternatePageController openPage(String fxmlPath) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fxmlPath));
            fxmlLoader.load();
            AlternatePageController controller = fxmlLoader.getController();
            controller.initializeController(this, getReturnPage());
            Pane root = fxmlLoader.getRoot();
            mainBorderPane.setCenter(root);
            return controller;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error loading view at " + fxmlPath);
        }
        return null;
    }

    /* Gets the current page being shown */
    Node getReturnPage() {
        return mainBorderPane.getCenter();
    }

    /* Makes a NoteView from the given Note and adds it to the given column */
    void addNoteToColumn(Note note, int columnIndex) {
        NoteColumnView column = noteColumns.get(columnIndex);
        NoteView view = new NoteView(this, column,
                note.getName(), note.getDescription(), note.getLabels());
        column.addToEnd(view);
        note.setObserver(view);
        noteModel.addNote(view, note);
    }

    /* Makes a TodoView from the given To-do and adds it to the given column */
    void addTodoToColumn(Todo todo, int columnIndex) {
        TodoColumnView column = todoColumns.get(columnIndex);
        TodoView view = new TodoView(this, column,
                todo.getName(), todo.getDescription(), todo.getLabels(),
                todo.getDate(), todo.getPriority(), todo.getSubNotes(), todo.getSubTodos());
        column.addToEnd(view);
        todo.setObserver(view);
        noteModel.addTodo(view, todo);
    }

    /* Opens a new page to edit the given note */
    void editItem(NoteView view) {
        if (view instanceof TodoView) {
            Todo todo = noteModel.getTodoModel((TodoView)view);
            NewTodoController newTodoController = (NewTodoController) openPage(PageType.TODO);
            if(newTodoController != null) newTodoController.initEditMode(todo, view.getColumnName());
        } else {
            Note note = noteModel.getNoteModel(view);
            NewNoteController newNoteController = (NewNoteController) openPage(PageType.NOTE);
            if(newNoteController != null) newNoteController.initEditMode(note, view.getColumnName());
        }
    }

    /* Opens a new page to edit the given note with another controller observing it */
    void editItem(Note note, DisplayNoteController controller) {
        if (note instanceof Todo) {
            Todo todo = (Todo)note;
            NewTodoController newTodoController = (NewTodoController) openPage(PageType.TODO);
            if(newTodoController != null) {
                newTodoController.setObserver(controller);
                newTodoController.initEditMode(todo, todo.getColumnName());
            }
        } else {
            NewNoteController newNoteController = (NewNoteController) openPage(PageType.NOTE);
            if(newNoteController != null) newNoteController.initEditMode(note, note.getColumnName());
        }
    }

    /* Gets the names of all the note columns */
    private ArrayList<String> getNoteColumnNames() {
        ArrayList<String> names = new ArrayList<>();
        for(NoteColumnView column : noteColumns) names.add(column.getName());
        return names;
    }

    /* Gets the names of all the to-do columns */
    private ArrayList<String> getTodoColumnNames() {
        ArrayList<String> names = new ArrayList<>();
        for(TodoColumnView column : todoColumns) names.add(column.getName());
        return names;
    }

    /* Resets the current page to be the given page */
    void resetMainPane(Node returnPane) {
        mainBorderPane.setCenter(returnPane);
    }

    /* START LABEL MODEL INTERACTIONS */
    public ArrayList<LabelView> getLabels() {
        return labelModel.getLabelsSorted();
    }

    void changeLabelColor(LabelView label, Color color) {
        labelModel.changeLabelColor(label, color);
    }

    void changeLabelTextColor(LabelView label, Color color) {
        labelModel.changeLabelTextColor(label, color);
    }

    void changeLabelName(LabelView label, String name) {
        labelModel.changeLabelName(label, name);
    }

    void deleteLabels(LabelView label) {
        labelModel.deleteLabels(label);
    }

    ArrayList<LabelView> copyLabels(ArrayList<LabelView> view, Note note, boolean addAsObserver) {
        return labelModel.copyLabels(view, note, addAsObserver);
    }
    /* END LABEL MODEL INTERACTIONS */

    /* Creates a pop up to get label specifications from the user */
    public void addNewLabel() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setGraphic(null);
        dialog.setHeaderText("Name the label:");
        dialog.setTitle("New Label");
        ColorPicker colorPicker = new ColorPicker();
        colorPicker.setValue(Color.DODGERBLUE);
        ColorPicker textColorPicker = new ColorPicker();
        textColorPicker.setValue(Color.BLACK);
        Text bg = new Text("Background color: ");
        Text txt = new Text("Text color:");
        HBox bgBox = new HBox(bg, colorPicker);
        HBox txtBox = new HBox(txt, textColorPicker);
        bgBox.setSpacing(10.0);
        txtBox.setSpacing(10.0);
        TextField textField = new TextField();
        VBox vBox = new VBox(textField, bgBox, txtBox);
        vBox.setSpacing(10.0);
        dialog.getDialogPane().setContent(vBox);
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(name -> labelModel.addLabel(textField.getText(), colorPicker.getValue(), textColorPicker.getValue()));
    }

    /* START NOTE MODEL INTERACTIONS */
    Todo getTodoModel(TodoView view) {
        return noteModel.getTodoModel(view);
    }

    private Note getNoteModel(NoteView view) {
        return noteModel.getNoteModel(view);
    }

    ArrayList<Note> getNotes() {
        return noteModel.getNotesSorted();
    }

    ArrayList<Todo> getTodos() {
        return noteModel.getTodosSorted();
    }
    /* END NOTE MODEL INTERACTIONS */

    /* Removes the given to-do from its column and then puts it in completed pane */
    void markAsCompleted(TodoView view){
        view.columnView.removeItemView(view);
        completedTabPane.getChildren().add(view);
        view.initCompletedEditButton();
    }

    /* Move the given to-do back into its old column */
    void unmarkCompleted(TodoView view){
        completedTabPane.getChildren().remove(view);
        view.columnView.addToEnd(view);
        view.initEditButton();
    }


    /* START CLICK AND DRAG */
    void setMovingView(NoteView view){
        movingView = view;
        movingView.prevColumn = view.columnView;
    }

    void setMovingColumn(ColumnView column){
        if(movingView != null){
            if(movingView.prevColumn != column){
                movingColumn = column;
            }
        }
    }

    void changeColumn(){
        if(movingView != null && movingColumn != null){
            if(movingView.prevColumn != movingColumn){
                movingView.columnView = movingColumn;
                movingView.prevColumn.removeItemView(movingView);
                movingColumn.addToBeginning(movingView);
                movingView.prevColumn = null;
                movingView = null;
            }
        }
    }
    /* END CLICK AND DRAG */
}
