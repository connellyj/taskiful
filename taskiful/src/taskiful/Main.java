package taskiful;

/* The starting point for this javaFX application */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import java.io.IOException;

public class Main extends Application {

    @Override
    /* Called when the application is launched */
    public void start(Stage stage) {
        // Sets the application icon
        Image img = new Image(getClass().getResourceAsStream("/img/taskIcon.png"));
        stage.getIcons().add(img);

        // Loads the main page
        Pane root;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/mainView.fxml"));
            Scene scene = new Scene(root, 1000, 600);
            stage.setTitle("Taskiful");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            System.err.println("Error opening mainView.fxml");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
