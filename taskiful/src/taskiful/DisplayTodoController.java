package taskiful;

/* The controller for displayTodoView.fxml. Fills in the available fields
 * and responds to user input coming from the view. Extends from
 * DisplayNoteController.java
 */

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import java.io.IOException;
import java.util.ArrayList;

public class DisplayTodoController extends DisplayNoteController {

    @FXML private VBox notesPane;
    @FXML private VBox todosPane;
    @FXML private Text priorityText;
    @FXML private Text dateText;
    private Todo todo;

    public void initializeController(SessionController sessionController, Node returnPane, Note note) {
        this.todo = (Todo)note;
        super.initializeController(sessionController, returnPane, note);
    }

    /* Fills in all available fields with data if the data exists */
    protected void initView() {
        super.initView();
        if(todo != null) {
            if (todo.getPriority() != null) priorityText.setText("Priority: " + todo.getPriority());
            if (todo.getDate() != null) dateText.setText("Due Date: " + todo.getDate());
            clearSubItems();
            if (todo.getSubNotes() != null && todo.getSubNotes().size() > 0) initNotes(todo.getSubNotes());
            if (todo.getSubTodos() != null && todo.getSubTodos().size() > 0) initTodos(todo.getSubTodos());
        }
    }

    /* Clears the sub-items displays */
    private void clearSubItems() {
        notesPane.getChildren().clear();
        todosPane.getChildren().clear();
    }

    /* Adds all sub-notes to the view */
    private void initNotes(ArrayList<Note> notes) {
        for(Note n : notes) addNoteToView(n);
    }

    /* Adds all sub-todos to the view */
    private void initTodos(ArrayList<Todo> todos) {
        for(Todo t : todos) addTodoToView(t);
    }

    /* Adds a single sub-note to the view */
    private void addNoteToView(Note note) {
        notesPane.getChildren().add(getItemDisplay(note, SessionController.DISPLAY_NOTE_PATH));
    }

    /* Adds a single sub-to-do to the view */
    private void addTodoToView(Todo todo) {
        todosPane.getChildren().add(getItemDisplay(todo, SessionController.DISPLAY_TODO_PATH));
    }

    /* Sets up a display and its controller in a modified way so that it can be
     * displayed within this view rather than on its own page
     */
    private Pane getItemDisplay(Note note, String fxmlPath) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fxmlPath));
            fxmlLoader.load();
            DisplayNoteController controller = fxmlLoader.getController();
            controller.initializeController(sessionController, sessionController.getReturnPage(), note);
            controller.deActivateBackButton();
            controller.setParentTodo(this);
            controller.changeDeleteToRemove(this.todo);
            return fxmlLoader.getRoot();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("error loading view at " + fxmlPath);
        }
        return null;
    }
}
