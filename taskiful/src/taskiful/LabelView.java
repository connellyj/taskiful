package taskiful;

/* Visually represents a label */

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

public class LabelView extends HBox {

    private String name;
    private Label label;
    private Color color;
    private Color textColor;

    LabelView(String labelText, Color color, Color textColor) {
        initialize(labelText);
        setColor(color);
        setTextColor(textColor);
    }

    /* Initialize style for the view and put the name in it */
    private void initialize(String labelText) {
        label = new Label();
        setName(labelText);
        label.setPadding(new Insets(1.0, 3.0, 1.0, 3.0));
        this.getChildren().add(label);
        this.setPadding(new Insets(5.0, 5.0, 5.0, 5.0));
    }

    /* Gets the text in this label */
    public String getName() {
        return name;
    }

    /* Sets the name of the label to a shortened version if necessary and adds a tooltip */
    public void setName(String name) {
        this.name = name;
        Tooltip tooltip = new Tooltip(name);
        if(name.length() >= 20) {
            this.name = name.substring(0, 18);
            this.name += "...";
        }
        label.setText(this.name);
        Tooltip.install(label, tooltip);
    }

    /* Labels are equal to each other if they have the same text */
    boolean equalTo(LabelView labelView) {
        return name.equals(labelView.getName());
    }

    /* Gets the background color of this label */
    Color getColor() {
        return this.color;
    }

    /* Sets the background color of this label */
    void setColor(Color color) {
        this.color = color;
        label.setBackground(new Background(new BackgroundFill(color, new CornerRadii(5.0),Insets.EMPTY)));
    }

    /* Gets the text color of this label */
    Color getTextColor() {
        return this.textColor;
    }

    /* Sets the text color of this label */
    void setTextColor(Color color) {
        this.textColor = color;
        label.setTextFill(textColor);
    }
}
