Taskiful

@Authors Julia Connelly and Jon Bisila

Taskiful is a to-do manager. It was designed with the objective of being able to create a to-do manager that is flexible
for users and allows for the ability to sort the todos and choose their own to-do management experience. It is designed
to be intuitive to use and currently has the following functionality:

1) Ability to add new notes and todos.
2) Ability to customize the following for a to-do:
   - Priority
   - Due Date
   - Labels (Now with Color!)
   - Column
   - Attach notes and subtasks in the form of other todos
3) Users have the ability to click and drag todos/notes from one column to another.
4) Users can modify their labels to have particular highlight and text colors.
5) User can add sub-notes and sub-todos to to-dos to allow for more flexible organizing


Known Bugs and Issues:
1) Click and drag functionality is a bit "sticky". This may be due to slowness in the JavaFX library.
2) When editing labels, their note/to-do boxes don't always resize to fit them well.
3) Can't rearrange columns into a different order.


To run the program, run Main in the Taskiful directory. There is some code written to show example use of taskiful,
but we would also recommend removing that code and testing from a blank slate.